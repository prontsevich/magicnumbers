﻿using UnityEngine;

public class MagicNumbers : MonoBehaviour
{
    public int stepsCount;
    public int min;
    public int max;
    public int result = 500;

    // Start is called before the first frame update
    void Start()
    {
        StartGame();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            max = result;
            CheckGuess();
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                min = result;
                CheckGuess();
            }
            else
            {
                if (Input.GetKeyDown(KeyCode.Return))
                {
                    print($"I know your number! It's {result}");
                    print($"It took {stepsCount} to guess your number");
                    StartGame();
                }
            }
        }
    }

    private void CheckGuess()
    {
        CalculateGuess(min, max);
        print($"Is it {result}");
    }

    private void CalculateGuess(int min, int max)
    {
        result = (min + max) / 2;
        stepsCount++;
    }

    private void StartGame()
    {
        min = 1;
        max = 1000;
        stepsCount = 0;
        CalculateGuess(min, max);
        print("Welcome to Magic Numbers");
        print($"Min {min}");
        print($"Max {max}");
        print($"Is your number {result}?");
    }
}
