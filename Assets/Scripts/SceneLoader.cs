﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void LoadNextScene()
    {
        var currentScene = SceneManager.GetActiveScene();
        var nextSceneIndex = currentScene.buildIndex + 1 < SceneManager.sceneCountInBuildSettings ? currentScene.buildIndex + 1 : 0;
        SceneManager.LoadScene(nextSceneIndex);
    }
}
