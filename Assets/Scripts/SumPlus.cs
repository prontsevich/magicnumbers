﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SumPlus : MonoBehaviour
{
    public int stepsCount;
    public int sum;
    private readonly List<KeyCode> _acceptedKeys = new List<KeyCode> { KeyCode.Space, KeyCode.Alpha1, KeyCode.Alpha2, KeyCode.Alpha3, KeyCode.Alpha4, KeyCode.Alpha5, KeyCode.Alpha6, KeyCode.Alpha7, KeyCode.Alpha8, KeyCode.Alpha9, KeyCode.Alpha0 };
    private const int KeyCodeIndex = 48;

    // Start is called before the first frame update
    void Start()
    {
        StartGame();
    }

    // Update is called once per frame
    void Update()
    {
        var keysDict = _acceptedKeys.ToDictionary(x => x, Input.GetKeyDown);

        if (keysDict.Any(x => x.Value))
        {
            var pressedKey = keysDict.First(x => x.Value).Key;
            switch (pressedKey)
            {
                case KeyCode.Space:
                    sum = 0;
                    break;
                default:
                    sum += (int) pressedKey - KeyCodeIndex;
                    stepsCount++;
                    break;
            }
            Debug.Log($"Sum = {sum}");
        }

        if (sum >= 50)
        {
            Debug.Log($"Ready! {stepsCount} steps.");
            StartGame();
        }
    }

    private void StartGame()
    {
        stepsCount = 0;
        sum = 0;
        Debug.Log($"Welcome to Sum!");
        Debug.Log($"Press 0-9 to sum or Space to reset.");
    }
}
